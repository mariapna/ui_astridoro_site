(function(){
	$(document).ready(function(){
		$('.carrossel-clientes').slick({
			dots: false,
			speed: 450,
			slidesToShow: 5,
			slidesToScroll: 1,
			autoplay: false,
			autoplaySpeed: 2000,
			arrows: false,
			// responsive: [
			// {
			// 	breakpoint: 1024,
			// 	settings: {
			// 		slidesToShow: 3,
			// 		slidesToScroll: 3,
			// 		infinite: true,
			// 		dots: true
			// 	}
			// },
			// {
			// 	breakpoint: 600,
			// 	settings: {
			// 		slidesToShow: 2,
			// 		slidesToScroll: 2
			// 	}
			// },
			// {
			// 	breakpoint: 480,
			// 	settings: {
			// 		slidesToShow: 1,
			// 		slidesToScroll: 1
			// 	}
			// }
		 //    You can unslick at a given breakpoint now by adding:
		 //    settings: "unslick"
		 //    instead of a settings object
		 //    ]
		});
	});

	$('.pg-produtos .pop-up-produto span.close-button').click(function(){
		$('.pg-produtos .pop-up').removeClass('open-pop-up');
		$('body').removeClass('stop-scroll');
	});

	$('.pg-produtos .secao-produtos .lista-produtos li').click(function(){
		$('.pg-produtos .pop-up').addClass('open-pop-up');
		$('body').addClass('stop-scroll');
	});

}());